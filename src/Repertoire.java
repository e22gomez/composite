import java.util.ArrayList;

public class Repertoire extends Element {
    private ArrayList<Element> childs;

    public Repertoire(String name){
        super(name);
        this.childs = new ArrayList<>();
        this.name = name;
    }
    public void ajouterElem(Element elem){
        if(!this.childs.contains(elem)){
            elem.path = this.path + "/" +  elem.name;
            childs.add(elem);
            System.out.println("Ajout effectué");
        }else {
            System.out.println("Ajout impossible");
        }
    }

    public void supprimerElem(Element elem){
        childs.remove(elem);
    }

    public ArrayList<Element> acqElem(){
        return this.childs;
    }

    public int getSize(){
        for(Element elem : this.childs){
            this.size += elem.getSize();
        }
        return this.size;
    }

}
