import java.util.ArrayList;

public class RepertoireAmovible extends Repertoire {
    private ArrayList<Element> childs;
    private int limitedSize;

    public RepertoireAmovible(int limitedSize, String name){
        super(name);
        this.limitedSize = limitedSize;
        this.childs= new ArrayList<>();
        this.name = name;
    }

    public void ajouterElem(Element elem){
        if(this.size + elem.size > limitedSize){
            System.out.println("Stockage insuffisant");
        }
        else {
            childs.add(elem);
            System.out.println("Ajout effectué");
        }
    }

    public void supprimerElem(Element elem){
        childs.remove(elem);
    }

    public ArrayList<Element> acqElem(){
        return this.childs;
    }

    public int getSize(){
        for(Element elem : this.childs){
            this.size += elem.getSize();
        }
        return this.size;
    }

}
