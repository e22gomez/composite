public class Main {
    public static void main(String[] args) {
        Repertoire r1 = new Repertoire("repertoire1");
        Repertoire r2 = new Repertoire("repertoire2");
        RepertoireAmovible ra1 = new RepertoireAmovible(10, "repertoireAmovible1");
        Fichier f1 = new Fichier("name1");
        Fichier f2 = new Fichier("name2");
        Fichier f3 = new Fichier("name3");

        r2.ajouterElem(f1);
        r2.ajouterElem(f2);
        r2.ajouterElem(f3);
        r1.ajouterElem(r2);

        r2.ajouterElem(f1);

        ra1.ajouterElem(new Fichier("1"));

        System.out.println(r1.getSize() + "ko");
        System.out.println(f1.path);
        System.out.println(r2.acqElem());
    }
}
