import java.util.Random;

public class Fichier extends Element{
    public Fichier(String name){
        super(name);
        Random random = new Random();
        this.size = random.nextInt(0,100);
    }

    @Override
    public int getSize() {
        return this.size;
    }
}
