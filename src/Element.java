public abstract class Element {
    protected int size;
    protected String name;
    protected String path;

    public Element(String name){
        size = 0;
        this.name = name;
        this.path = "/" + name;
    }

    public abstract int getSize();

    public String toString(){
        return this.name;
    }
}
